function Countdown(options) {
  var timer,
  instance = this,
  seconds = options.seconds || 10,
  updateStatus = options.onUpdateStatus || function () {},
  counterEnd = options.onCounterEnd || function () {
};
function decrementCounter() {
    updateStatus(seconds);
    if (seconds === 0) {
      counterEnd();
      instance.stop();
    }
    seconds--;
}
this.start = function () {
    clearInterval(timer);
    timer = 0;
    seconds = options.seconds;
    timer = setInterval(decrementCounter, 1000);
};
this.stop = function () {
    clearInterval(timer);
  };
}
/* This is the Context Menu.*/
var redRockTimerSettings = chrome.contextMenus.create({
	"title": "Red Rock Timer Settings"
});
var fiveMinutes = chrome.contextMenus.create({
	"title": "5 Minutes", "parentId": redRockTimerSettings, "onclick": start5
});
var tenMinutes = chrome.contextMenus.create({
	"title": "10 Minutes", "parentId": redRockTimerSettings, "onclick": start10
});
var twentyMinutes = chrome.contextMenus.create({
	"title": "20 Minutes", "parentId": redRockTimerSettings, "onclick": start20
});
var thirtyMinutes = chrome.contextMenus.create({
	"title": "30 Minutes", "parentId": redRockTimerSettings, "onclick": start30
});
var fortyMinutes = chrome.contextMenus.create({
	"title": "40 Minutes", "parentId": redRockTimerSettings, "onclick": start40
});
var sixtyMinutes = chrome.contextMenus.create({
	"title": "60 Minutes", "parentId": redRockTimerSettings, "onclick": start60
});
var myCounterFiveMinutes = new Countdown({  
    seconds:300,  // number of seconds to count down
    onUpdateStatus: function(sec){
		//console.log(sec);
	if (sec === 250) {
		chrome.browserAction.setIcon({path:"../img/logo_active1.png"});
	}
	if (sec === 150) {
		chrome.browserAction.setIcon({path:"../img/logo_active2.png"});
		}
	if (sec === 100) {
		chrome.browserAction.setIcon({path:"../img/logo_active3.png"});
		}
	if (sec === 50) {
		chrome.browserAction.setIcon({path:"../img/logo_active4.png"});
		}
		}, // callback for each second
	onCounterEnd: function(){ 
	showFiveMinutes();
	chrome.browserAction.setIcon({path:"../img/logo_24_idle.png"});
	} // final action
});
function start5() {
	chrome.browserAction.setIcon({path:"../img/logo_active.png"});
	myCounterFiveMinutes.start();
}
function start10() {
	chrome.browserAction.setIcon({path:"../img/logo_active.png"});
	myCounterTenMinutes.start();
}
function start20() {
	chrome.browserAction.setIcon({path:"../img/logo_active.png"});
	myCounterTwentyMinutes.start();
}
function start30() {
	chrome.browserAction.setIcon({path:"../img/logo_active.png"});
	myCounterThirtyMinutes.start();
}
function start40() {
	chrome.browserAction.setIcon({path:"../img/logo_active.png"});
	myCounterFortyMinutes.start();
}
function start60() {
	chrome.browserAction.setIcon({path:"../img/logo_active.png"});
	myCounterSixtyMinutes.start();
}
var myCounterTwentyMinutes = new Countdown({  
    seconds:1200,  // number of seconds to count down
    onUpdateStatus: function(sec){
	//console.log(sec);
	if (sec === 800) {
		chrome.browserAction.setIcon({path:"../img/logo_active1.png"});
	}
	if (sec === 450) {
		chrome.browserAction.setIcon({path:"../img/logo_active2.png"});
	}
	if (sec === 100) {
		chrome.browserAction.setIcon({path:"../img/logo_active3.png"});
	}
	if (sec === 50) {
		chrome.browserAction.setIcon({path:"../img/logo_active4.png"});
	}
	}, // callback for each second
    onCounterEnd: function(){ 
	showTenMinutes();
	chrome.browserAction.setIcon({path:"../img/logo_24_idle.png"});
	} // final action
});
var myCounterThirtyMinutes = new Countdown({  
    seconds:2400,  // number of seconds to count down
    onUpdateStatus: function(sec){
	//console.log(sec);
	if (sec === 1500) {
		chrome.browserAction.setIcon({path:"../img/logo_active1.png"});
	}
	if (sec === 800) {
		chrome.browserAction.setIcon({path:"../img/logo_active2.png"});
	}
	if (sec === 200) {
		chrome.browserAction.setIcon({path:"../img/logo_active3.png"});
	}
	if (sec === 50) {
		chrome.browserAction.setIcon({path:"../img/logo_active4.png"});
	}
	}, // callback for each second
    onCounterEnd: function(){ 
	showTenMinutes();
	chrome.browserAction.setIcon({path:"../img/logo_24_idle.png"});
	} // final action
});
var myCounterFortyMinutes = new Countdown({  
    seconds:4800,  // number of seconds to count down
    onUpdateStatus: function(sec){
	//console.log(sec);
	if (sec === 2400) {
		chrome.browserAction.setIcon({path:"../img/logo_active1.png"});
	}
	if (sec === 1200) {
		chrome.browserAction.setIcon({path:"../img/logo_active2.png"});
	}
	if (sec === 400) {
		chrome.browserAction.setIcon({path:"../img/logo_active3.png"});
	}
	if (sec === 50) {
		chrome.browserAction.setIcon({path:"../img/logo_active4.png"});
	}
	}, // callback for each second
    onCounterEnd: function(){ 
	showTenMinutes();
	chrome.browserAction.setIcon({path:"../img/logo_24_idle.png"});
	} // final action
});
var myCounterSixtyMinutes = new Countdown({  
    seconds:9600,  // number of seconds to count down
    onUpdateStatus: function(sec){
	//console.log(sec);
	if (sec === 2500) {
		chrome.browserAction.setIcon({path:"../img/logo_active1.png"});
	}
	if (sec === 650) {
		chrome.browserAction.setIcon({path:"../img/logo_active2.png"});
	}
	if (sec === 400) {
		chrome.browserAction.setIcon({path:"../img/logo_active3.png"});
	}
	if (sec === 50) {
		chrome.browserAction.setIcon({path:"../img/logo_active4.png"});
	}
	}, // callback for each second
    onCounterEnd: function(){ 
	showTenMinutes();
	chrome.browserAction.setIcon({path:"../img/logo_24_idle.png"});
	} // final action
});
var myCounterTenMinutes = new Countdown({  
    seconds:600,  // number of seconds to count down
    onUpdateStatus: function(sec){
	//console.log(sec);
	if (sec === 500) {
		chrome.browserAction.setIcon({path:"../img/logo_active1.png"});
	}
	if (sec === 350) {
		chrome.browserAction.setIcon({path:"../img/logo_active2.png"});
	}
	if (sec === 150) {
		chrome.browserAction.setIcon({path:"../img/logo_active3.png"});
	}
	if (sec === 50) {
		chrome.browserAction.setIcon({path:"../img/logo_active4.png"});
	}
	}, // callback for each second
    onCounterEnd: function(){ 
	showTenMinutes();
	chrome.browserAction.setIcon({path:"../img/logo_24_idle.png"});
	} // final action
});
function showFiveMinutes() {
	  var time = /(..)(:..)/.exec(new Date());
	  var hour = time[1] % 12 || 12;
	  var period = time[1] < 12 ? 'AM' : 'PM';
	  new Notification(hour + time[2] + ' ' + period, {
	    icon: '../img/notify.png',
	    body: 'Your five (5) minutes are up!'
	});
}
function showTenMinutes() {
	  var time = /(..)(:..)/.exec(new Date());
	  var hour = time[1] % 12 || 12;
	  var period = time[1] < 12 ? 'AM' : 'PM';
	  new Notification(hour + time[2] + ' ' + period, {
	    icon: '../img/notify.png',
	    body: 'Your ten (10) minutes are up!'
	});
}
function showTwentyMinutes() {
	  var time = /(..)(:..)/.exec(new Date());
	  var hour = time[1] % 12 || 12;
	  var period = time[1] < 12 ? 'AM' : 'PM';
	  new Notification(hour + time[2] + ' ' + period, {
	    icon: '../img/notify.png',
	    body: 'Your twenty (20) minutes are up!'
	});
}
function showThirtyMinutes() {
	  var time = /(..)(:..)/.exec(new Date());
	  var hour = time[1] % 12 || 12;
	  var period = time[1] < 12 ? 'AM' : 'PM';
	  new Notification(hour + time[2] + ' ' + period, {
	    icon: '../img/notify.png',
	    body: 'Your thirty (30) minutes are up!'
	});
}
function showFortyMinutes() {
	  var time = /(..)(:..)/.exec(new Date());
	  var hour = time[1] % 12 || 12;
	  var period = time[1] < 12 ? 'AM' : 'PM';
	  new Notification(hour + time[2] + ' ' + period, {
	    icon: '../img/notify.png',
	    body: 'Your forty (40) minutes are up!'
	});
}
function showSixtyMinutes() {
	  var time = /(..)(:..)/.exec(new Date());
	  var hour = time[1] % 12 || 12;
	  var period = time[1] < 12 ? 'AM' : 'PM';
	  new Notification(hour + time[2] + ' ' + period, {
	    icon: '../img/notify.png',
	    body: 'Your sixty (60) minutes are up!'
	});
}