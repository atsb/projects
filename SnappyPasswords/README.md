# SnappyPasswords
Snappy Passwords was a small Chrome web store app that I made a few years ago.  The app would generate quick passwords.

There was an Android version too that generated securely salted passwords, but I lost the source.
