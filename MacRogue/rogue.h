/*
 * Copyright (c) 1988, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Timoth C. Stoehr.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)rogue.h	8.1 (Berkeley) 5/31/93
 * $FreeBSD: src/games/rogue/rogue.h,v 1.4 2001/01/25 12:24:29 phantom Exp $
 */

#include <curses.h>
#include <string.h>
#include <stdlib.h>

/*
 * rogue.h
 *
 * This source herein may be modified and/or distributed by anybody who
 * so desires, with the following restrictions:
 *    1.)  This notice shall not be removed.
 *    2.)  Credit shall not be taken for the creation of this source.
 *    3.)  This code is not to be traded, sold, or used for personal
 *         gain or profit.
 */

#define boolean char

#define NOTHING		((unsigned short)     0)
#define OBJECT		((unsigned short)    01)
#define MONSTER		((unsigned short)    02)
#define STAIRS		((unsigned short)    04)
#define HORWALL		((unsigned short)   010)
#define VERTWALL	((unsigned short)   020)
#define DOOR		((unsigned short)   040)
#define FLOOR		((unsigned short)  0100)
#define TUNNEL		((unsigned short)  0200)
#define TRAP		((unsigned short)  0400)
#define HIDDEN		((unsigned short) 01000)

#define ARMOR		((unsigned short)   01)
#define WEAPON		((unsigned short)   02)
#define SCROL		((unsigned short)   04)
#define POTION		((unsigned short)  010)
#define GOLD		((unsigned short)  020)
#define FOOD		((unsigned short)  040)
#define WAND		((unsigned short) 0100)
#define RING		((unsigned short) 0200)
#define AMULET		((unsigned short) 0400)
#define ALL_OBJECTS	((unsigned short) 0777)

#define LEATHER 0
#define RINGMAIL 1
#define SCALE 2
#define CHAIN 3
#define BANDED 4
#define SPLINT 5
#define PLATE 6
#define ARMORS 7

#define BOW 0
#define DART 1
#define ARROW 2
#define DAGGER 3
#define SHURIKEN 4
#define MACE 5
#define LONG_SWORD 6
#define TWO_HANDED_SWORD 7
#define WEAPONS 8

#define MAX_PACK_COUNT 24

#define PROTECT_ARMOR 0
#define HOLD_MONSTER 1
#define ENCH_WEAPON 2
#define ENCH_ARMOR 3
#define IDENTIFY 4
#define TELEPORT 5
#define SLEEP 6
#define SCARE_MONSTER 7
#define REMOVE_CURSE 8
#define CREATE_MONSTER 9
#define AGGRAVATE_MONSTER 10
#define MAGIC_MAPPING 11
#define CON_MON 12
#define SCROLS 13

#define INCREASE_STRENGTH 0
#define RESTORE_STRENGTH 1
#define HEALING 2
#define EXTRA_HEALING 3
#define POISON 4
#define RAISE_LEVEL 5
#define BLINDNESS 6
#define HALLUCINATION 7
#define DETECT_MONSTER 8
#define DETECT_OBJECTS 9
#define CONFUSION 10
#define LEVITATION 11
#define HASTE_SELF 12
#define SEE_INVISIBLE 13
#define POTIONS 14

#define TELE_AWAY 0
#define SLOW_MONSTER 1
#define INVISIBILITY 2
#define POLYMORPH 3
#define HASTE_MONSTER 4
#define MAGIC_MISSILE 5
#define CANCELLATION 6
#define DO_NOTHING 7
#define DRAIN_LIFE 8
#define COLD 9
#define FIRE 10
#define WANDS 11

#define STEALTH 0
#define R_TELEPORT 1
#define REGENERATION 2
#define SLOW_DIGEST 3
#define ADD_STRENGTH 4
#define SUSTAIN_STRENGTH 5
#define DEXTERITY 6
#define ADORNMENT 7
#define R_SEE_INVISIBLE 8
#define MAINTAIN_ARMOR 9
#define SEARCHING 10
#define RINGS 11

#define RATION 0
#define FRUIT 1

#define NOT_USED		((unsigned short)   0)
#define BEING_WIELDED	((unsigned short)  01)
#define BEING_WORN		((unsigned short)  02)
#define ON_LEFT_HAND	((unsigned short)  04)
#define ON_RIGHT_HAND	((unsigned short) 010)
#define ON_EITHER_HAND	((unsigned short) 014)
#define BEING_USED		((unsigned short) 017)

#define NO_TRAP -1
#define TRAP_DOOR 0
#define BEAR_TRAP 1
#define TELE_TRAP 2
#define DART_TRAP 3
#define SLEEPING_GAS_TRAP 4
#define RUST_TRAP 5
#define TRAPS 6

#define STEALTH_FACTOR 3
#define R_TELE_PERCENT 8

#define UNIDENTIFIED ((unsigned short) 00)	/* MUST BE ZERO! */
#define IDENTIFIED ((unsigned short) 01)
#define CALLED ((unsigned short) 02)

#define DROWS 24
#define DCOLS 80
#define NMESSAGES 5
#define MAX_TITLE_LENGTH 30
#define MAXSYLLABLES 40
#define MAX_METAL 14
#define WAND_MATERIALS 30
#define GEMS 14

#define GOLD_PERCENT 46

#define MAX_OPT_LEN 40

struct id {
	short value;
	char *title;
	char *real;
	unsigned short id_status;
};

/* The following #defines provide more meaningful names for some of the
 * struct object fields that are used for monsters.  This, since each monster
 * and object (scrolls, potions, etc) are represented by a struct object.
 * Ideally, this should be handled by some kind of union structure.
 */

#define m_damage damage
#define hp_to_kill quantity
#define m_char ichar
#define first_level is_protected
#define last_level is_cursed
#define m_hit_chance class
#define stationary_damage identified
#define drop_percent which_kind
#define trail_char d_enchant
#define slowed_toggle quiver
#define moves_confused hit_enchant
#define nap_length picked_up
#define disguise what_is
#define next_monster next_object

struct obj {				/* comment is monster meaning */
	unsigned long m_flags;	/* monster flags */
	const char *damage;		/* damage it does */
	short quantity;			/* hit points to kill */
	short ichar;			/* 'A' is for aquatar */
	short kill_exp;			/* exp for killing it */
	short is_protected;		/* level starts */
	short is_cursed;		/* level ends */
	short class;			/* chance of hitting you */
	short identified;		/* 'F' damage, 1,2,3... */
	unsigned short which_kind; /* item carry/drop % */
	short o_row, o_col, o;	/* o is how many times stuck at o_row, o_col */
	short row, col;			/* current row, col */
	short d_enchant;		/* room char when detect_monster */
	short quiver;			/* monster slowed toggle */
	short trow, tcol;		/* target row, col */
	short hit_enchant;		/* how many moves is confused */
	unsigned short what_is;	/* imitator's charactor (?!%: */
	short picked_up;		/* sleep from wand of sleep */
	unsigned short in_use_flags;
	struct obj *next_object;	/* next monster */
};

typedef struct obj object;

#define INIT_AW (object*)0,(object*)0
#define INIT_RINGS (object*)0,(object*)0
#define INIT_HP 12,12
#define INIT_STR 16,16
#define INIT_EXP 1,0
#define INIT_PACK {0}
#define INIT_GOLD 0
#define INIT_CHAR '@'
#define INIT_MOVES 1250

struct fightr {
	object *armor;
	object *weapon;
	object *left_ring, *right_ring;
	short hp_current;
	short hp_max;
	short str_current;
	short str_max;
	object pack;
	uint32_t gold;
	short exp;
	long exp_points;
	short row, col;
	short fchar;
	short moves_left;
};

typedef struct fightr fighter;

struct dr {
	short oth_room;
	short oth_row,
	      oth_col;
	short door_row,
		  door_col;
};

typedef struct dr door;

struct rm {
	short bottom_row, right_col, left_col, top_row;
	door doors[4];
	unsigned short is_room;
};

typedef struct rm room;

#define MAXROOMS 9
#define BIG_ROOM 10

#define NO_ROOM -1

#define PASSAGE -3		/* cur_room value */

#define AMULET_LEVEL 26

#define R_NOTHING	((unsigned short) 01)
#define R_ROOM		((unsigned short) 02)
#define R_MAZE		((unsigned short) 04)
#define R_DEADEND	((unsigned short) 010)
#define R_CROSS		((unsigned short) 020)

#define MAX_EXP_LEVEL 21
#define MAX_EXP 10000001L
#define MAX_GOLD 999999
#define MAX_ARMOR 99
#define MAX_HP 999
#define MAX_STRENGTH 99
#define LAST_DUNGEON 99

#define STAT_LEVEL 01
#define STAT_GOLD 02
#define STAT_HP 04
#define STAT_STRENGTH 010
#define STAT_ARMOR 020
#define STAT_EXP 040
#define STAT_HUNGER 0100
#define STAT_LABEL 0200
#define STAT_ALL 0377

#define PARTY_TIME 10	/* one party somewhere in each 10 level span */

#define MAX_TRAPS 10	/* maximum traps per level */

#define HIDE_PERCENT 12

struct tr {
	short trap_type;
	short trap_row, trap_col;
};

typedef struct tr trap;

extern fighter rogue;
extern room rooms[];
extern trap traps[];
extern unsigned short dungeon[DROWS][DCOLS];
extern object level_objects;

extern struct id id_scrolls[];
extern struct id id_potions[];
extern struct id id_wands[];
extern struct id id_rings[];
extern struct id id_weapons[];
extern struct id id_armors[];

extern object mon_tab[];
extern object level_monsters;

#define MONSTERS 26

#define HASTED					01L
#define SLOWED					02L
#define INVISIBLE				04L
#define ASLEEP				   010L
#define WAKENS				   020L
#define WANDERS				   040L
#define FLIES				  0100L
#define FLITS				  0200L
#define CAN_FLIT			  0400L		/* can, but usually doesn't, flit */
#define CONFUSED	 		 01000L
#define RUSTS				 02000L
#define HOLDS				 04000L
#define FREEZES				010000L
#define STEALS_GOLD			020000L
#define STEALS_ITEM			040000L
#define STINGS			   0100000L
#define DRAINS_LIFE		   0200000L
#define DROPS_LEVEL		   0400000L
#define SEEKS_GOLD		  01000000L
#define FREEZING_ROGUE	  02000000L
#define RUST_VANISHED	  04000000L
#define CONFUSES		 010000000L
#define IMITATES		 020000000L
#define FLAMES			 040000000L
#define STATIONARY		0100000000L		/* damage will be 1,2,3,... */
#define NAPPING			0200000000L		/* can't wake up for a while */
#define ALREADY_MOVED	0400000000L

#define SPECIAL_HIT		(RUSTS|HOLDS|FREEZES|STEALS_GOLD|STEALS_ITEM|STINGS|DRAINS_LIFE|DROPS_LEVEL)

#define WAKE_PERCENT 45
#define FLIT_PERCENT 40
#define PARTY_WAKE_PERCENT 75

#define HYPOTHERMIA 1
#define STARVATION 2
#define POISON_DART 3
#define QUIT 4
#define WIN 5
#define KFIRE 6

#define UPWARD 0
#define UPRIGHT 1
#define RIGHT 2
#define DOWNRIGHT 3
#define DOWN 4
#define DOWNLEFT 5
#define LEFT 6
#define UPLEFT 7
#define DIRS 8

#define ROW1 7
#define ROW2 15

#define COL1 26
#define COL2 52

#define MOVED 0
#define MOVE_FAILED -1
#define STOPPED_ON_SOMETHING -2
#define CANCEL '\033'
#define LIST '*'

#define HUNGRY 300
#define WEAK 150
#define FAINT 20
#define STARVE 0

#define MIN_ROW 1

/* external routine declarations.
 */
const char *mon_name(const object *);
const char *get_ench_color(void);
const char *name_of(const object *);
const char *md_gln(void);
char *md_getenv(const char *name);
char *md_malloc(int n);
boolean mask_pack(void);
boolean mask_room(void);
boolean is_digit(void);
boolean md_df(const char *fname);
boolean has_been_touched(void);
object *alloc_object(void);
object *get_thrown_at_monster(void);
object *get_zapped_monster(void);
object *check_duplicate(void);
object *gr_object(void);
object *pick_up(int, int, short);
struct id *get_id_table(object*);
unsigned short gr_what_is(void);
#define rrandom rand
#define srrandom(x) random()
long lget_number(const char *s);
void byebye(void);
void onintr(void);
void error_save(void);
int rgetchar(void);
int get_rand(int, int);
void zapp(void);
void sound_bell(void);
void message(const char *, boolean);
void check_message(void);
int pack_letter(const char *, unsigned short);
void bounce(short, short, short, short, short);
void special_hit(object *monster);
void wdrain_life(object *);
void wake_up(object *);
void put_mons(void);
int rand_percent(int);
void rust(object *);
void freeze(object *);
void zap_monster(object *, unsigned short);
void md_ignore_signals(void);
void md_heed_signals(void);
void md_control_keybord(char *mode);
int md_get_file_id(const char *fname);
int md_link_count(const char *fname);
void md_gct(struct rogue_time *rt_buf);
void md_gfmt(const char *fname, struct rogue_time *rt_buf);
int save_into_file(const char *sfile);
int init(int argc, char *argv[]);
int clean_up(const char *estr);
int xxx(int st);
void r_write(FILE *fp, const char *buf, int n);
int is_vowel(short ch);
int save_game(void);
int get_input_line(const char *prompt, const char *insert, char *buf, const char *if_cancelled, int add_blank, int do_echo);
void write_string(char *s, FILE *fp);
void write_pack(const object *pack, FILE *fp);
void rw_dungeon(FILE *fp, int rw);
void rw_id(struct id id_table[], FILE *fp, int n, int wr);
void rw_rooms(FILE *fp, int rw);
void r_read(FILE *fp, char *buf, int n);
void do_args(int argc, char *argv[]);
void do_opts(void);
void start_window(void);
void put_scores(const object *monster, short other);
int md_gseed(void);
int md_exit(int status);
int md_lock(int l);
void md_shell(const char *shell[]);
void restore(const char *fname);
void mix_colors(void);
int get_desc(const object *obj, char *desc);
int get_armor_class(const object *obj);
object *add_to_pack(object *obj, object *pack, int condense);
int can_move(int row1, int col1, int row2, int col2);
void tele(void);
int is_direction(short c, short *d);
int gr_dir(void);
void light_up_room(int rn);
void wake_room(short rn, int entering, short row, short col);
int mv_1_monster(object *monster, short row, short col);
int mon_can_go(const object *monster, short row, short col);
void get_dir_rc(short dir, short row, short col, short allow_off_screen);
void rogue_hit(object *monster, boolean force_hit);
int check_imitator(object *monster);
void print_stats(int stat_mask);
void disappear(object *monster);
int get_dungeon_char(int row, int col);
void vanish(object *obj, short rm, object *pack);
void free_object(object *obj);
void take_from_pack(object *obj, object *pack);
int rogue_can_see(int row, int col);
int gmc_row_col(int row, int col);
void place_at(object *obj, int row, int col);
object *get_letter_object(int ch);
object *object_at(object *pack, short row, short col);
void plant_gold(short row, short col, int is_maze);
int put_objects(void);
void put_objects_2(void);
int coin_toss(void);
void make_party(void);
int mv_mons(void);
void killed_by(const object *monster, short other);
int light_passage(int row, int col);
int steal_gold(object *monster);
int steal_item(object *monster);
void freeze(object *monster);
int drain_life(void);
void darken_room(int rn);
int imitating(short row, short col);
int get_room_number(int row, int col);
int trap_player(short row, short col);
int next_to_something(int drow, int dcol);
void turn_passage(short dir, int fast);
void multiple_move_rogue(short dirch);
int is_passable(int row, int col);
void move_onto(void);
int check_hunger(int msg_only);
int reg_move(void);
void wanderer(void);
void unhallucinate(void);
void hallucinate(void);
void unblind(void);
void unconfuse(void);
int heal(void);
int search(short n, boolean is_auto);
void rest(int count);
object *gr_monster(object *monster, int mn);
int gr_obj_char(void);
void gr_row_col(short *row, short *col, unsigned short mask);
void put_m_at(short row, short col, object *monster);
int no_room_for_monster(int rn);
int rogue_is_around(int row, int col);
int m_confuse(object *monster);
int cough_up(object *monster);
int make_room(short rn, short r1, short r2, short r3);
void add_mazes(void);
void make_level(void);
void mix_random_rooms(void);
int connect_rooms(short room1, short room2);
int same_row(int room1, int room2);
int same_col(int room1, int room2);
int is_all_connected(void);
void visit_rooms(int rn);
char get_mask_char(unsigned short mask);
int dr_course(object *monster, int entering, short row, short col);
int mon_sees(const object *monster, int row, int col);
void mon_hit(object *monster);
int flame_broil(object *monster);
int drop_level(void);
int try_to_cough(short row, short col, object *obj);
int show_monsters(void);
void rand_around(short i, short *r, short *c);
void aim_monster(object *monster);
int rogue_can_see(int row, int col);
int move_confused(object *monster);
int flit(object *monster);
void mv_aquatars(void);
const char *mon_name(const object *monster);
void wake_room(short rn, int entering, short row, short col);
void wake_up(object *monster);
void move_mon_to(object *monster, short row, short col);
int mtry(object *monster, short row, short col);
int seek_gold(object *monster);
int gold_at(short row, short col);
void check_gold_seeker(object *monster);
int sting(object *monster);
int hp_raise(void);
void add_exp(int e, int promotion);
int get_dir(short srow, short scol, short drow, short dcol);
void fill_out_level(void);
int party_objects(int rn);
int gr_room(void);
int get_oth_room(short rn, short *row, short *col);
void edit_opts(void);
void opt_show(int i);
void opt_erase(int i);
void opt_go(int i);
void do_shell(void);
void wait_for_ack(void);

struct rogue_time {
	short year;		/* >= 1987 */
	short month;	/* 1 - 12 */
	short day;		/* 1 - 31 */
	short hour;		/* 0 - 23 */
	short minute;	/* 0 - 59 */
	short second;	/* 0 - 59 */
};

