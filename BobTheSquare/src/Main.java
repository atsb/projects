import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Main extends StateBasedGame{
   
   public static final String gamename = "Bob the Square";
   public static final String version = "v0.5 alpha";
   public static final int play_game = 0;
   public static final int xSize = 800;
   public static final int ySize = 600;
   
   public Main(String gamename){
      super(gamename);
      this.addState(new Game());
   }
   
   public void initStatesList(GameContainer gc) throws SlickException{
      this.getState(play_game).init(gc, this);
      this.enterState(play_game);
   }
   
   public static void main(String[] args) {
      AppGameContainer appgc;
      try{
         appgc = new AppGameContainer(new Main(gamename + " - " + version));
         appgc.setDisplayMode(xSize, ySize, false);
         appgc.setTargetFrameRate(60);
         appgc.start();
      }catch(SlickException e){
         e.printStackTrace();
      }
   }
}