import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.tiled.TiledMap;

import java.awt.Font;
import java.io.InputStream;


import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import org.newdawn.slick.util.ResourceLoader;

public class Game extends BasicGameState {
	Image bob_the_square_front;
	Image bob_the_square_side;
	Image bob_the_square_other_side;
	Image bob_the_square_back;
	Image maze_border_top_bottom;
	Image maze_border;
	Image maze_lava;
	Image maze_box;
	Image maze_floor;
	Image maze_floor_side;
	Image maze_floor_top;

	private TiledMap map;
	
	Vector2f currentPosition = new Vector2f();
	float xPos = 100;
	float yPos = 90;
	float scale = 1;
	
	public Game() {

	}

	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
		bob_the_square_front = new Image("assets/bobthesquare_front.png");
		maze_border_top_bottom = new Image("assets/maze_wall_top_bottom.png");
		maze_lava = new Image("assets/lava.png");
		maze_box = new Image("assets/box.png");
		maze_floor = new Image("assets/maze_floor.png");
		maze_floor_side = new Image("assets/maze_floor_side.png");
		maze_floor_top = new Image("assets/maze_floor_top.png");
		maze_border = new Image("assets/maze_wall.png");
		map = new TiledMap("assets/level1.tmx","assets");
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		bob_the_square_front.draw(xPos,yPos,scale);
		map.render(100,90);
		}

	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		/***
		 * The input here is for 50 pixels each way.
		 * This is the size of a tile.
		 * and only once per keypress.
		 */
		Input input = gc.getInput();
        if (input.isKeyPressed(Input.KEY_W)) {
        	yPos = yPos - 50;
            currentPosition.setY(currentPosition.getY() - 1);
        }
        if (input.isKeyPressed(Input.KEY_S)) {
        	yPos = yPos + 50;
            currentPosition.setY(currentPosition.getY() + 1);
        }
        if (input.isKeyPressed(Input.KEY_A)) {
        	xPos = xPos - 50;
            currentPosition.setY(currentPosition.getX() - 1);
        }
        if (input.isKeyPressed(Input.KEY_D)) {
        	xPos = xPos + 50;
            currentPosition.setX(currentPosition.getX() + 1);
        }
        if (input.isKeyPressed(Input.KEY_P)) {
            //print = true;
        }
        if (input.isKeyPressed(Input.KEY_Q)) {
            System.exit(0);
        }
	}

	public int getID() {
		return 0;
	}
}