# Rolly2D
Rolly2D was a small physics puzzle game I created in Torque2D back in 2011.

The zip file contains the source code.  I made this in Torque2D 1.7.5 (a heavily customised source version of it) and I can't be bothered to make it work in Torque2D 4 that now exists, it has changed quite a lot since those times.
