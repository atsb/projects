# projects

These are all my smaller projects that I work on, neatly placed in this one repository.  It is much easier for me to manage.

If anything interests you, please, fork it and use it.  In the spirit of FOSS :)


If you wish to change or otherwise place an issue or PR, please do so and list the project.  I am happy
to collaborate with anyone in order to change or fix things.